package com.example.sampleappdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleAppDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleAppDemoApplication.class, args);
	}
}
